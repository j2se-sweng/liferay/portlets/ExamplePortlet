<%@ include file="/init.jsp" %>


<%-- name="mvcPath" obbligatorio --%>


<liferay-portlet:actionURL name="prova" varImpl="provaActionURL">

<liferay-portlet:param name="numero" value="<%= String.valueOf(123456) %>" />

</liferay-portlet:actionURL>

<aui:a href="<%= provaActionURL.toString()  %>">
	link a actionURL
</aui:a>




<liferay-ui:search-container delta="2">

	<liferay-ui:search-iterator>

		<liferay-ui:search-container-results>
			<% 	results =  UserLocalServiceUtil.getUsers(searchContainer.getStart(),searchContainer.getEnd());

				total = UserLocalServiceUtil.getUsersCount();

				pageContext.setAttribute("results", results);
				pageContext.setAttribute("total", total);

			%>
		</liferay-ui:search-container-results>

		<!-- modelVar : Bean da cui prendere la proprietÓ specificata in keyProperty  -->

			<liferay-ui:search-container-row
				className="com.liferay.portal.model.User"
				keyProperty="userId"
				modelVar="user2"
			>

			<liferay-portlet:renderURL var="viewDetailsURL">
				<liferay-portlet:param name="mvcPath" value="/view_user.jsp" />
				<liferay-portlet:param name="userId" value="<%= String.valueOf(user2.getUserId()) %>" />
				<liferay-portlet:param name="redirect" value="<%= currentURL %>" />
			</liferay-portlet:renderURL>

		<%-- name  : nome della colonna  --%>
		<%-- property : nome della proprietÓ nel bean (anche nel db?) --%>
		<%-- value : Valori presi "al volo"  --%>


				<liferay-ui:search-container-column-text
					name="email-address"
					property="emailAddress"
				/>

				<%-- value="<%= user2.getFirstName() + \" \" + user2.getLastName() %>" --%>

				<liferay-ui:search-container-column-text
					href="<%= viewDetailsURL.toString() %>"
					name="first-name"
					property="firstName"
				/>

				<liferay-ui:search-container-column-text
					name="last-name"
					property="lastName"
				/>

			</liferay-ui:search-container-row>

	</liferay-ui:search-iterator>

</liferay-ui:search-container>

<%--
<%
	int uc = UserLocalServiceUtil.getUsersCount();

	List<User> users = UserLocalServiceUtil.getUsers(0, uc);

	for(User user_ : users) {
%>

	<%=	user_.getFullName() %>
<%
	}
%>
 --%>

<br/>This is the <b>ExamplePortlet</b> portlet.