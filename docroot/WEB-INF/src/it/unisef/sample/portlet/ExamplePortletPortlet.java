package it.unisef.sample.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import it.unisef.sample.util.WebKeys;

public class ExamplePortletPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

			try {
					User user = null;

					long userId = ParamUtil.getLong(request, "userId");

					if (userId > 0) {
							user = UserLocalServiceUtil.getUser(userId);
					}

					request.setAttribute(WebKeys.SAMPLE_USER, user);
			}
			catch (Exception e) {
				throw new PortletException(e);
			}

		super.render(request, response);

		System.out.println("render()");
	}

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		System.out.println("processAction()");

		super.processAction(actionRequest, actionResponse);


	}

	public void prova(ActionRequest request, ActionResponse response) {


		_log.info("prova()");

		long numero = ParamUtil.getLong(request, "numero");

		_log.info("numero = " + numero );
	}

	private static Log _log = LogFactoryUtil.getLog(ExamplePortletPortlet.class);

}
